package contronl;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ObjectMapper mapper =new ObjectMapper();
    //3.登录 从数据库中验证用户明 和 密码是否正确  并创建会话 保存到会话中
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        if (username==null||"".equals(username)||password==null||"".equals(password)){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("请输入用户明或密码");
            return;
        }
        UserDao userDao = new UserDao();
        User user = userDao.selectByName(username);
        if (user==null||!user.getPassword().equals(password)){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户名或密码错误");
            return;
        }
        //创建会话
        HttpSession session = req.getSession(true);
        session.setAttribute("user",user);

        //4.跳转到主页
        resp.sendRedirect("blog_list.html");

    }

    //4. 获取登录信息(检测用户是否登录) 用来判断列表页 和详情页 的头像
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf8");
        HttpSession session = req.getSession(false);
        if (session==null){
            User user = new User();
            String us = mapper.writeValueAsString(user);
            resp.getWriter().write(us);
            return;
        }
        User user = (User) session.getAttribute("user");
        if (user==null){
            user = new User();
            String us = mapper.writeValueAsString(user);
            resp.getWriter().write(us);
            return;
        }

        user.setPassword("");
        resp.getWriter().write(mapper.writeValueAsString(user));
    }
}
