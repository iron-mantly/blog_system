package contronl;

import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/blogDelete")
public class BlogDeleteServlet extends HttpServlet {

    //8. 用来删除一篇博客
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //要想删除博客 就要先检验是否登录
        HttpSession session = req.getSession(false);
        if (session==null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户尚未登录,不能删除");
            return;
        }
        User user = (User) session.getAttribute("user");
        if (user==null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户尚未登录,不能删除");
            return;
        }

        String parameter = req.getParameter("blogId");
        if (parameter==null||"".equals(parameter)){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前参数不对");
            return;
        }
        //获取删除博客信息
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(Integer.parseInt(parameter));
        if (blog==null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前要删除的博客不存在");
            return;
        }
        if (user.getUserId()!=blog.getUserId()){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前登录的不是作者,没有权限删除");
            return;
        }
        //删除博客
        blogDao.delete(Integer.parseInt(parameter));
        //跳转到列表页
        resp.sendRedirect("blog_list.html");
    }
}
