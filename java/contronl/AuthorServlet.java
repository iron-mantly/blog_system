package contronl;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/authorInfo")
public class AuthorServlet extends HttpServlet {

    // 用来获取 详情页的 作者信息 前端页面并更换作者信息
    private ObjectMapper mapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        String parameter = req.getParameter("blogId");
        if (parameter==null||"".equals(parameter)){
            resp.getWriter().write("{\"ok\":false ,\"reason\":\"参数缺失\"}");
            return;
        }

        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(Integer.parseInt(parameter));
        if (blog==null){
            resp.getWriter().write("{\"ok\":false ,\"reason\":\"要查询的博客不存在\"}");
            return;
        }

        UserDao userDao = new UserDao();
        User user = userDao.selectById(blog.getUserId());
        if (user==null){
            resp.getWriter().write("{\"ok\":false ,\"reason\":\"要查询的博客不存在\"}");
            return;
        }
        //把对象 构造为json字符串
        resp.getWriter().write(mapper.writeValueAsString(user));

    }
}
