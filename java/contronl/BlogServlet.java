package contronl;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    // 第1.这个方法用来获取到数据库中的博客列表.
    //2. 如果含有 blogid 也可以获取详情页 内容
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 从数据库中查询到博客列表, 转成 JSON 格式, 然后直接返回即可.
        String para = req.getParameter("blogId");
        resp.setContentType("application/json; charset=utf8");
        BlogDao blogDao = new BlogDao();
        if (para==null){
            List<Blog> blogs = blogDao.selectAll();
            // 把 blogs 对象转成 JSON 格式.
            String respJson = objectMapper.writeValueAsString(blogs);

            resp.getWriter().write(respJson);
        }else{
            Blog blog = blogDao.selectOne(Integer.parseInt(para));
            String respJson = objectMapper.writeValueAsString(blog);
            resp.getWriter().write(respJson);
        }

    }

    //7. form 表单来发布博客 存到数据库中  发布博客
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取session 判断当前作者是否登录
        HttpSession session = req.getSession(false);
        if (session==null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录");
            return;
        }
        User user = (User) session.getAttribute("user");
        if (user==null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录");
            return;
        }
        req.setCharacterEncoding("utf8");
        String title = req.getParameter("title");
        String content = req.getParameter("content");

        if (title==null||"".equals(title)||content==null||"".equals(content)){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("提交博客失败 缺少必要的参数");
            return;
        }

        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());
        BlogDao blogDao = new BlogDao();
        blogDao.insert(blog);

        resp.sendRedirect("blog_list.html");
    }
}
